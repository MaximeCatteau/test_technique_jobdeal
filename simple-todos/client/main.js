import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
 
import '../imports/startup/accounts-config.js';
import App from '../imports/ui/App.js';
import App2 from '../imports/ui/App2.js';
 
Meteor.startup(() => {
  render(<App2 />, document.getElementById('render-target'));
});


