import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Entries = new Mongo.Collection('entries');

if(Meteor.isServer){
  Meteor.publish('entries', function tasksPublication() {
    return Entries.find();
  });

Meteor.methods({
  'entries.insert'(date, value){
    check(date, String);
    check(value, String);

    if(!this.userId){
      throw new Meteor.Error('not-authorized');
    }

    Entries.insert({
//      date: new Date().toString(),
      date,
      value,
      createdAt: new Date(),
      owner: this.userId,
      username: Meteor.users.findOne(this.userId).username,
    });
  }
});

}
