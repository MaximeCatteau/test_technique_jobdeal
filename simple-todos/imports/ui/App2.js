import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { withTracker} from 'meteor/react-meteor-data';

import { Entries } from '../api/entries.js';

import Entry from './Entry.js';
import AccountsUIWrapper from './AccountsUIWrapper.js';

import ReactTable from 'react-table';
import 'react-table/react-table.css';

import { Form } from 'react-bootstrap';
import { FormGroup } from 'react-bootstrap';
import { ControlLabel } from 'react-bootstrap';
import { FormControl } from 'react-bootstrap';

import { Line } from 'react-chartjs-2';

/*const donnees = {
    labels: {this.props.dates},
    datasets: [
      {
	label: 'Charts with React',
	fill: false,
	lineTension: 0.1,
	backgroundColor: 'rgba(75,192,192,0.4)',
	borderColor: 'rgba(75,192,192,1)',
	borderCapStyle: 'butt',
	borderDash: [],
      }
    ]
};*/


var abscisse = [];
var ordonnee = [];
var tmpdates;
var tmpval;
var obj;
var theValues;


class App2 extends Component {

  handleSubmit(event){
    event.preventDefault();

    const val = ReactDOM.findDOMNode(this.refs.textval).value.trim();
    const date = ReactDOM.findDOMNode(this.refs.textdate).value.trim();

    console.log(val);

    if(val === ''){
      alert('Missing value');
    }
    else {
      if(date === ''){
        var today = new Date();
        date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        console.log(date);
      }
      console.log("Trying to insert");
      Meteor.call('entries.insert', date, val);
      ReactDOM.findDOMNode(this.refs.textval).value = '';
      ReactDOM.findDOMNode(this.refs.textdate).value = '';
    }
  }

  getColumns(){
    return [
      {
	Header: 'Date',
	accessor: 'date',
      },
      {
	Header: 'Value',
	accessor: 'value',
      },
    ]
  }

  renderEntries(){
    return this.props.entries.map((entry) => (
      <Entry key={entry._id} date={date} valeur={valeur} />
    ));
  }

  render(){
   tmpdates = this.props.dates;
   for (x in tmpdates){
     console.log(tmpdates[x].date);
     abscisse.push(tmpdates[x].date);
     console.log(tmpdates[x].value);
     ordonnee.push(parseInt(tmpdates[x].value));
   }

   console.log(ordonnee);

/*   tmpval = this.props.values;
   
   console.log(tmpval);
   for(x in tmpval){
     toint.push(parseInt(tmpval[x].value));
   }

   ordonnee = toint;

   for(x in ordonnee){
     console.log(ordonnee[x]);
   }*/ 
   theValues = [42,13,20,43,12,15,99,13,6];
   console.log(theValues);
    return (
      <div className="container">
	<header>
	  <h1>Tableau ({this.props.count})</h1>

 	  <AccountsUIWrapper />

          { this.props.currentUser ?
	    <Form inline onSubmit = {this.handleSubmit.bind(this)}>
              <FormGroup controlId="formInlineDate">
                <ControlLabel>Date</ControlLabel>
                {' '}
                <FormControl type="date" ref="textdate" placeholder="dd/mm/yyyy"/>
                {' (if you don\'t put some date, it will be today\'s date)'}
              </FormGroup>
              {' '}
              <FormGroup controlId="formInlineValue">
		<ControlLabel>Value</ControlLabel>
		{' '}
                <FormControl type="number" ref="textval" placeholder="0"/>
       	      </FormGroup>
	      <button type="submit">
	        Ajouter
	      </button>
            </Form>: ''
            
         }
	</header>

	<ReactTable
	  data={this.props.entries}
          columns={this.getColumns()}
	/>
  
	<Line 
          data = {{
  labels: abscisse,
  datasets: [
            {
		label: 'Date / Value',
		fill: false,
		lineTension: 0.9,
		backgroundColor: 'rgba(75,192,192,0.4)',
		borderColor: 'rgba(75,192,192,1)',
		borderCapStyle: 'butt',
		borderDash: [],
		borderDashOffset: 0.0,
		borderJoinStyle: 'miter',
		pointBorderColor: 'rgba(75,192,192,1)',
		pointBackgroundColor: '#fff',
		pointBorderWidth: 1,
	        pointHoverRadius: 5,
	        pointHoverBackgroundColor: 'rgba(75,192,192,1)',
 	        pointHoverBorderColor: 'rgba(220,220,220,1)',
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: ordonnee
	    }]
}}
        />
      </div>
    )
  }
}

export default withTracker(() => {
  Meteor.subscribe('entries');
  return {
    entries: Entries.find({},{"date":1, "value":1, "_id":0, sort: { "createdAt": -1}}).fetch(),

    // Compte le nombre d'entrées dans la base de donnée
    count: Entries.find({}).count(),
    currentUser: Meteor.user(),
    dates: Entries.find({}, {"date":1, "_id":0}).fetch(),
    values: Entries.find({}, {"value":1, "_id":0}).fetch(),
  }
})(App2);
