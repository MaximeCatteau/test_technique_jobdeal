import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';

import { Entries } from '../api/entries.js'
 
// Entry component - represents a single entry
export default class Entry extends Component {

  deleteThisEntry(){
    Entries.remove(this.props.entry._id);
  }
  render() {
    console.log("In Entry !");

    return (
      <div>{this.props.entry.valeur}
        <button className="delete" onClick={this.deleteThisEntry.bind(this)}>
          &times;
        </button>
        <span className="text">{this.props.entries.valeur}</span>
      </div>
    );
  }
}


